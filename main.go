package main

import (
	_ "embed"
	"errors"
	"flag"
	"fmt"
	"html/template"
	"io"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"runtime"
	"strings"
	"time"
)

var (
	color string
)

// TODO : allow larger than memory transfers

//go:embed root.html
var mainHtml string

type Server struct {
	p  string
	k  string
	d  string
	kb bool
}

func (s Server) serve() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		ts := struct {
			Title string
			Color string
			Key   bool
		}{Title: s.d,
			Color: color,
			Key:   s.kb}
		t := template.New("t0")
		if _, err := t.Parse(mainHtml); err != nil {
			log.Printf("ERROR::t.Parse(mainHtml)::%v\n", err)
			http.Error(w, err.Error(), 500)
			return
		}
		if err := t.Execute(w, &ts); err != nil {
			log.Printf("ERROR::t.Execute(w, &ts)::%v\n", err)
			http.Error(w, err.Error(), 500)
			return
		}
	})
	http.HandleFunc("/upload", func(w http.ResponseWriter, r *http.Request) {
		if s.kb {
			fmt.Println("access key required")
			if r.FormValue("key") == s.k {
				fmt.Println("access key accepted")
			} else {
				fmt.Println("access was denied to ", r.RemoteAddr)
				return
			}
		}
		var uploadStatus = ""
		if err := r.ParseMultipartForm(32 << 20); err != nil {
			log.Printf("ERROR::uploadFunc()::r.ParseMultipartForm::%v", err)
		}
		formFile, handler, _ := r.FormFile("fileToUpload")
		var location string
		fileName := handler.Filename
		if strings.Contains(fileName, "/") || strings.Contains(fileName, "\\") {
			if _, err := fmt.Fprint(w, "ERROR::Filename contains directory char"); err != nil {
				return
			}
			return
		}
		location = s.d + fileName
		log.Printf("UPLOAD::%s\n", location)
		f, _ := os.OpenFile(location, os.O_WRONLY|os.O_CREATE, 0666)
		uploadStatus = "file_" + fileName + "_uploaded"
		if _, err := io.Copy(f, formFile); err != nil {
			uploadStatus = fmt.Sprintf("ERROR::io.Copy()::%v\n", err)
			return
		}
		if err := formFile.Close(); err != nil {
			uploadStatus = fmt.Sprintf("ERROR::formFile.Close()::%v\n", err)
			return
		}
		if err := f.Close(); err != nil {
			uploadStatus = fmt.Sprintf("ERROR::f.Close()::%v\n", err)
			return
		}
		if _, err := fmt.Fprint(w, uploadStatus); err != nil {
			log.Printf("ERROR::fmt.Fprint(w, ...)::%v", err)
		}
	})
	fmt.Printf("http://%s:%s\n", func() string {
		a, err := net.InterfaceAddrs()
		if err != nil {
			log.Printf("ERROR::net.InterfaceAddrs()::%v\n", err)
			return ""
		}
		for _, address := range a {
			if ip, ok := address.(*net.IPNet); ok && !ip.IP.IsLoopback() {
				if ip.IP.To4() != nil {
					return ip.IP.String()
				}
			}
		}
		return ""
	}(), s.p)
	if err := http.ListenAndServe(":"+s.p, nil); err != nil {
		log.Printf("ERROR::http.ListenAndServe()::%v\n", err)
	}
}

func main() {
	s := Server{}
	dir, err := os.Getwd()
	if err != nil {
		log.Panicf("ERROR::os.Getwd()::%v\n", err)
	}
	if runtime.GOOS == "windows" {
		if dir[:len(dir)-1] != "\\" {
			dir = dir + "\\"
		}
	} else {
		if dir[:len(dir)-1] != "/" {
			dir = dir + "/"
		}
	}
	flag.StringVar(&color, "c", "#00ff00", "css color")
	flag.StringVar(&s.d, "d", dir, "target directory")
	flag.StringVar(&s.p, "p", "8236", "server port")
	flag.BoolVar(&s.kb, "k", false, "use access key for access")
	flag.ErrHelp = errors.New("help text")
	flag.Parse()
	if s.kb {
		keyLength := 32
		rand.Seed(time.Now().UnixNano())
		chars := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
		var b strings.Builder
		for kl := 0; kl < keyLength; kl++ {
			ci := rand.Intn(len(chars))
			b.WriteRune(chars[ci])
		}
		s.k = b.String()
		fmt.Printf("ACCESSKEY: %s\n", s.k)
	}
	fmt.Printf("Target Directory: %s\n", dir)
	s.serve()
}
